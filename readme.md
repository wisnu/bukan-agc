# bukan AGC
### this plugin don't do anything

---

**Required plugins**

* [Image Sizes](https://wordpress.org/plugins/image-sizes/) : to disable generate thumbs

**Ready Themes**

* [theme](https://bitbucket.org/wisnu/ready-themes/src/master/)




### Using it
---

* edit ads.php on plugin folder
* add your keyword.txt to plugin folder. Available keywords are below (others will be randomly generated):
 - [home](http://pool.wis.nu/content/listAvailableKeywords/home_en)
 - [resume](http://pool.wis.nu/content/listAvailableKeywords/resume)
* Simply activate on plugins page.
* Check all Image Size option (every template changes)
![](https://2.bp.blogspot.com/-8cTjlWmxEjc/W618zcx1omI/AAAAAAAAADU/I7tsd3CR8XcLnTErEqvvnrqnCdvaw3lvACLcBGAs/s1600/Screen%2BShot%2B2018-09-28%2Bat%2B7.15.15%2BAM.png)

* SSH to plugin folder and insert all post

```sh
cd ~/ROOT_WP/wp-content/plugins/bukan-agc
php poster.php insert {SOURCE} {CATEGORY} {NICHE}

php poster.php insert pinterest house home
```

* set perminute cron to publish unpublished posts (where 10 is total images will be attached)

```sh
* * * * * php /YOUR_WP_ROOT/wp-content/plugins/bukan-agc/poster.php attach {SOURCE} 10
```
_on runcloud, we can use this_

```sh
* * * * * /RunCloud/Packages/php70rc/bin/php /home/USER/webapps/wp_localsave/wp-content/plugins/bukan-agc/poster.php attach {SOURCE} 10
```


### Short Tutorial
---
* insert ads to theme
```php
<?php echo get_ads('responsive'); ?>
```

* insert random post permalinks
```php
<?php echo random_terms(10); ?>
```

* insert article
```php
<?php echo get_article(); ?>
```


### Troubleshoot
---
* Make sure permission and ownership was correct
* If no content, make sure wp-content/contents folder exists