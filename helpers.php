<?php


function get_ads($size) {
	$ads = include(WP_CONTENT_DIR . '/plugins/bukan-agc/ads.php');
	return $ads[$size];
}

function limit_text($text, $limit) {
	if (str_word_count($text, 0) > $limit) {
		$words = str_word_count($text, 2);
		$pos = array_keys($words);
		$text = substr($text, 0, $pos[$limit]) . '...';
	}
	return ucwords($text);
}

function random_terms($total) {
	$lines = file(WP_CONTENT_DIR . '/plugins/bukan-agc/keyword.txt');
	shuffle($lines);
	foreach( array_slice($lines, 0, $total) as $a) {
		$link .= '<li><a href="'.get_site_url().'/'.sanitize_title_with_dashes(remove_accents($a)).'">'.limit_text($a, 4).'</a></li>';
	}
	return $link;
}

function get_article() {
	$post = get_post();
	$filename = $_SERVER['HTTP_HOST'].'_'.$post->post_name;

	if (file_exists(WP_CONTENT_DIR . '/contents/'.$filename)) {
		$content = file_get_contents(WP_CONTENT_DIR . '/contents/'.$filename);
	} else {
		file_put_contents(WP_CONTENT_DIR . '/contents/'.$filename, file_get_contents('http://pool.wis.nu/article/keyword/'.$post->post_name));
		$content = file_get_contents(WP_CONTENT_DIR . '/contents/'.$filename);
	}

	return nl2br($content);
}

function get_attachment_article()
{
	if ( !is_attachment() )
		return false;
	$current_attachment = get_queried_object();
	$post = get_post($current_attachment->post_parent);
	$filename = $_SERVER['HTTP_HOST'].'_'.$post->post_name;

	if (file_exists(WP_CONTENT_DIR . '/contents/'.$filename)) {
		$content = file_get_contents(WP_CONTENT_DIR . '/contents/'.$filename);
	} else {
		file_put_contents(WP_CONTENT_DIR . '/contents/'.$filename, file_get_contents('http://pool.wis.nu/article/keyword/'.$post->post_name));
		$content = file_get_contents(WP_CONTENT_DIR . '/contents/'.$filename);
	}

	return nl2br($content);
}


function get($url) {
	$curlHandle = curl_init();
	curl_setopt($curlHandle, CURLOPT_URL, $url);
	curl_setopt($curlHandle, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curlHandle, CURLOPT_USERAGENT, file_get_contents('http://pool.wis.nu/ua/browser'));
	$response = curl_exec($curlHandle);
	curl_close($curlHandle);
	return $response;
}

function getFileExtension($url) {
	$extension = explode('.', $url);
	$extension = end($extension);
	if(empty($extension) || strlen($extension) > 4) {$extension = 'jpg';}
	return $extension;
}

function getRandomUserId(){
	global $wpdb;
	$listUser = $wpdb->get_col("SELECT ID FROM $wpdb->users");
	shuffle($listUser);
	return $listUser[0];
}

function createCategory($name){
	if(($term = get_term_by('name', $name, 'category')) != false) {
		return (int) $term->term_id;
	} else {
		$term = wp_insert_term($name,'category');
		return $term['term_id'];
	}
}

function optional(&$object, $default = null) {
	if(isset($object) && !empty($object)) {
		return $object;
	}
	return $default;
}

function post($string, $default = null) {
	return optional($_POST[$string], $default);
}


function addAttachmentContent($post_id, $attachment_id, $title) {
	$post = get_post($post_id);
	$attachment_link = get_permalink($attachment_id);

	$content = $post->post_content;

	$imgSrc = wp_get_attachment_image($attachment_id, 'full', false, array('alt' => $title));
	$html = "<a href=\"{$attachment_link}\">{$imgSrc}</a><p>{$title}<p>";

	$wp_error = wp_update_post(array(
			'ID'            => $post_id,
			'post_content'  => $content.$html
		), true);

	if(is_wp_error($wp_error)) {
		print($wp_error->get_error_message());
		print(PHP_EOL);
		/*
            $this->responseJson(array(
                'status'    => AGCPostProcessing::AGC_RETURN_OK,
                'message'   => $wp_error->get_error_message(),
                'code'      => 'attachment_failed_add_content'
            ));
*/
	}else{
		print("Added post content to Post ID {$post_id} with attachment ID {$attachment_id}");
		print(PHP_EOL);
	}
}



function nl2p($string, $line_breaks = true, $xml = true) {

	$string = str_replace(array('<p>', '</p>', '<br>', '<br />'), '', $string);

	// It is conceivable that people might still want single line-breaks
	// without breaking into a new paragraph.
	if ($line_breaks == true)
		return '<p>'.preg_replace(array("/([\n]{2,})/i", "/([^>])\n([^<])/i"), array("</p>\n<p>", '$1<br'.($xml == true ? ' /' : '').'>$2'), trim($string)).'</p>';
	else
		return '<p>'.preg_replace(
			array("/([\n]{2,})/i", "/([\r\n]{3,})/i","/([^>])\n([^<])/i"),
			array("</p>\n<p>", "</p>\n<p>", '$1<br'.($xml == true ? ' /' : '').'>$2'),

			trim($string)).'</p>';
}

function getContents($url) {
	return file_get_contents($url, false, stream_context_create(["http" => ['method' => 'GET','header' =>'X-Authorization: '.get_ads('apikey')]]));
}

function getRelated($keyword) {
	$opts = [
	"http" => [
	"method" => "GET",
	"header" => "Accept-language: en\r\n" .
		"Cookie: foo=bar\r\n" .
		"User-Agent: Mozilla/5.0 (X11; U; Linux i686; de-AT; rv:1.7.10) Gecko/20050722"
	]];

	$context = stream_context_create($opts);

	$relatedGo = json_decode(file_get_contents('http://suggestqueries.google.com/complete/search?client=firefox&q='.str_replace(' ', '%20' , $keyword).'&hl=en',false, $context));
	$related = array();
	foreach ($relatedGo[1] as $r1) {
		$related[] = ucfirst($r1);
	}

	return $related;
}


add_filter('get_ads', 'get_ads');
add_filter('limit_text', 'limit_text');
add_filter('random_terms', 'random_terms');
add_filter('get_article', 'get_article');
add_filter('get_attachment_article', 'get_attachment_article');
