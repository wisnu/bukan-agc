<?php
error_reporting(0);
require_once (__DIR__) . '/../../../wp-config.php';
require_once (__DIR__) . '/vendor/autoload.php';
require_once (__DIR__) . '/ads.php';
require_once (__DIR__) . '/helpers.php';

// $domain = $argv[2]; // domain

use Intervention\Image\ImageManager;
// use \Carbon;

global $wpdb;
date_default_timezone_set('Asia/Jakarta');
set_time_limit(3000);


$manager = new ImageManager(array('driver' => 'imagick'));
$act = isset($_GET['act']) ? $_GET['act'] :$argv[1];


//http://WPDOMAIN/wp-content/plugins/bukan-agc/poster.php?act=keyword&keyword=halloween
//php poster.php keyword scholarship
if ($act == 'keyword') {
	$keyword = isset($argv[2]) ? $argv[2] : $_GET['keyword'];
	$lists = getContents("http://pool.wis.nu/content/pinterestKeywords/$keyword");

	try {
		file_put_contents(dirname(__FILE__).'/keyword.txt', $lists);
	} catch (Exception $e) {
		var_dump($e);
	}
}

//http://WPDOMAIN/wp-content/plugins/bukan-agc/poster.php?act=insert&source=pinterest&category=home&niche=home_en
// php poster.php insert pinterest scholarship scholarship
if ($act == 'insert') {

	$lines = \file(dirname(__FILE__).'/keyword.txt', \FILE_IGNORE_NEW_LINES | \FILE_SKIP_EMPTY_LINES);
	$source = isset($argv[2]) ? $argv[2] : $_GET['source'];
	$category = isset($argv[3]) ? $argv[3] : $_GET['category'];


	foreach ($lines as $line) {
		$niche = isset($argv[4]) ? $argv[4] : $_GET['niche'];
		$slug = sanitize_title_with_dashes(remove_accents($line));

		$query = "SELECT $wpdb->posts.*
		FROM $wpdb->posts
		WHERE $wpdb->posts.post_name = '$slug'";

		$post_exists = $wpdb->get_row($query);

		if ($post_exists) {
			echo $line.' exists'.PHP_EOL;
		} else {
			echo $line.' will be posted now ...'.PHP_EOL;

			$tags = getRelated($line);
			$category = $category;
			if (!empty($niche)) {
				$content = getContents("http://pool.wis.nu/content/keyword/$line");
			} else {
				$content = getContents("http://pool.wis.nu/content/keyword/$line/$niche");
			}
			$post = array(
				'post_title'        => getContents("http://pool.wis.nu/content/randomizeTitle/$line"),
				'post_category'     => array(createCategory($category)),
				'post_name'         => $slug,
				'post_status'       => 'private',
				'post_author'       => getRandomUserId(),
				'post_excerpt'      => isset($content) ? limit_text(strip_tags($content), 200) : null,
				'post_date'         => \Carbon\Carbon::now()->subMinutes(rand(1, 832255)),
				'post_date_gmt'     => \Carbon\Carbon::now()->subMinutes(rand(1, 832255))
			);

			$post_id = wp_insert_post($post, true);

			if(is_wp_error($post_id)) {
				print_r($post_id->get_error_message());

			} else {
				wp_set_post_tags($post_id, $tags);
			}

		}

	}
}

//http://WPDOMAIN/wp-content/plugins/bukan-agc/poster.php?act=attach&source=pinterest&totalPost=2
// php poster.php attach pinterest 1
if ($act == 'attach') {

	$source = isset($argv[2]) ? $argv[2] : $_GET['source'];
	$totalPost = isset($argv[3]) ? $argv[3] : $_GET['totalPost'];

	$query = "SELECT $wpdb->posts.*
	FROM $wpdb->posts
	WHERE $wpdb->posts.post_status = 'private'";

	$post = $wpdb->get_row($query);

	$pid            = $post->ID;
	$parent_id      = $post->post_parent;
	if(!empty($pid)) {
		$parent_id = $pid;
	}
	echo $post->post_name.PHP_EOL;
	if ($source == 'google') {
		$json = json_decode(getContents('http://pool.wis.nu/content/scrapeGoogle/'.$post->post_name));
	} elseif ($source == 'pinterest') {
		$json = json_decode(getContents('http://pool.wis.nu/content/pinterestResult/'.$post->post_name));
	}
	$a = 0;

	for ($i=0; $i<$totalPost; $i++) {
// 	foreach ($json->data as $data) {
		$title          = $json->data[$i]->title;	//$data->title;
		$atitle         = $json->data[$i]->title;
		if(!empty($atitle)) {
			$title = $atitle;
		}
		$imageUrl       = $json->data[$i]->url; //$data->url;

		// check first
		$check = @get_headers($imageUrl, 1);
		if ($check[0] == 'HTTP/1.1 200 OK' || $check[0] == 'HTTP/1.1 301 Found') {
			$fileExt        = getFileExtension($imageUrl);
			$filename       = sanitize_title_with_dashes(remove_accents($title));

			$upload_dir = wp_upload_dir();
			$imagePath = $upload_dir['path'] . "/{$filename}.{$fileExt}";
			if(file_exists($imagePath)) {
				$filename = "$filename-".uniqid();
				$imagePath = $upload_dir['path'] . "/{$filename}.{$fileExt}";
			}

			/*
			// Standar save image
			$imageDump = get($imageUrl);
			$fileCheck = file_put_contents($imagePath, $imageDump);
		*/

			// Save using intervention
			$imageDump = $manager->make($imageUrl);
			$imageDump->getCore()->stripImage();
			$fileCheck = $imageDump->save($imagePath, 90);

			if(false != $fileCheck && ! empty($imageDump)) {
				$imageDump = null; // clear memory

				$guid = $upload_dir['url'] . "/{$filename}.{$fileExt}";
				if(strlen($guid) > 255) {
					// max = 255  - http://domian/wp-upload/ - jpg - ./
					$maxLengthFilename = 255 - strlen($upload_dir['url'] - strlen($fileExt) - 2);
					$filename = substr($filename, 0, $maxLengthFilename);
					$guid = $upload_dir['url'] . "/{$filename}.{$fileExt}";
				}

				$attachment = array(
					'guid'           => $guid,
					'post_mime_type' => "image/{$fileExt}",
					'post_title'     => $title,
					'post_status'    => 'inherit',
					'post_date'      => \Carbon\Carbon::now()->subMinutes(rand(1, 832255)),
					'post_excerpt'   => isset($data->caption) ? $data->caption : $data->description,
					'post_content'   => isset($data->description) ? $data->description : $data->caption,
				);

				$attachment_id = wp_insert_attachment($attachment, $imagePath, $parent_id, true);

				if( ! is_wp_error($attachment_id)) {
					require_once( ABSPATH . 'wp-admin/includes/image.php' );

					$attachment_data = wp_generate_attachment_metadata( $attachment_id, $imagePath );
					wp_update_attachment_metadata( $attachment_id, $attachment_data );

					set_post_thumbnail( $parent_id, $attachment_id );

					//         if(post('insert_content') == 'y') {
					/*
				if($argv[2] == 'y') {
					addAttachmentContent($parent_id, $attachment_id, $title);
				}
*/
					// response OK
					print("Success add attachment {$title} to post ID {$parent_id}");
					print(PHP_EOL);
					/*
				$this->responseJson(array(
						'status'        => AGCPostProcessing::AGC_RETURN_OK,
						'aid'           => $attachment_id,
						'attachment_id' => $attachment_id
					));
*/
				} else {
					print($attachment_id->get_error_message());
					/*
				$this->responseJson(array(
						'status'    => AGCPostProcessing::AGC_RETURN_ERROR,
						'message'   => $attachment_id->get_error_message(),
						'code'      => 'attachment_failed_create_attachment'
					));
*/
				}

			} elseif($fileCheck == false) {
				print("Failed to save image [{$imageUrl}]. Please check file permission / disk space");
				print(PHP_EOL);
				/*
			$this->responseJson(array(
					'status'    => AGCPostProcessing::AGC_RETURN_ERROR,
					'message'   => 'Failed to save image. Please check file permission / disk space',
					'code'      => 'attachment_save_failed'
				));
*/
			} elseif(empty($imageDump)) {
				print("Failed to download image. Image url return empty");
				print(PHP_EOL);
				/*
			$this->responseJson(array(
					'status'    => AGCPostProcessing::AGC_RETURN_ERROR,
					'message'   => 'Failed to download image. Image url return empty',
					'code'      => 'attachment_image_return_empty'
				));
*/
			}

			$post_id = wp_update_post(array('ID' => $pid, 'post_status' => 'publish'), true);
			if(is_wp_error($post_id)) {
				$query = "UPDATE {$wpdb->posts} SET post_status = 'publish' WHERE ID = {$pid}";
				$result = $wpdb->query($query);

				if($result === 0) {
					/*
                $this->responseJson(array(
                    'status'    => AGCPostProcessing::AGC_RETURN_ERROR,
                    'message'   => "Failed to Execute $query",
                    'code'      => 'attachment_failed_publish'
                ));
*/
				}else{
					print("Post [$post_id] success to publish by Query");
					print(PHP_EOL);
					/*
                $this->responseJson(array(
                    'status'    => AGCPostProcessing::AGC_RETURN_OK
                ));
*/
				}

			} else {
				print("Post [$post_id] success to publish");
				print(PHP_EOL);
				/*
            $this->responseJson(array(
                'status'    => AGCPostProcessing::AGC_RETURN_OK
            ));
*/
			}
		} else {
			print('NOK'.PHP_EOL);
			$a = $a + 1;
			
			// force delete failed keyword
			if ($a == $totalPost) {
				wp_delete_post( $pid,  true );
			}
		}

	}
	unset($a);
	unset($i);
	unset($post);
}

